Programmation Fonctionnelle en Coq : TD2
========================================

**M2 LMFI**

#### Exercice 1 : fonctions usuelles sur les entiers

Charger l'arithmétique unaire via `Require Import Arith`.

Si vous ne l'avez pas déjà fait la semaine dernière, définissez les fonctions suivantes sur le type `nat` (sans utiliser celles fournies par le système):

  - addition
  - multiplication
  - soustraction
  - factorielle
  - puissance
  - pgcd

#### Exercice 2 : Fibonacci

 - Définir une fonction `fib` telle que `fib 0 = 0`, `fib 1 = 1` puis `fib (n+2) = fib (n+1) + fib n`.
   (on pourra éventuellement utiliser un `as` dans les motifs du match).

 - Produire une version de `fib` nettement plus rapide en utilisant des paires.

 - Même question en utilisant juste des entiers, mais via une récursivité terminale.

 - Chargez la bibliothèque d'entiers binaire via `Require Import NArith`.
   Que faut-il changer à vos fonctions précédentes pour qu'elles soient maintenant de type `nat -> N` ?
   Quel impact cela a-t-il sur leur efficacité ?
   Peut-on en faire simplement des fonctions de type `N -> N` ?
   
#### Exercice 3 : Fibonacci matriciel

 - Définir un type des matrices 2x2 d'entiers (p.ex. via un quadruplet).
 - Définir une multiplication et une exponentiation de ces matrices.
   L'exponentiation pourra utiliser le type ̀positive`.
 - Définition une fonction fibonacci via l'exponentiation de la matrice suivante
 
```
1 1
1 0
```

#### Exercice 4 : écriture en base Fibonacci

On cherche ici à mettre en pratique le théorème de Zeckendorf : tout entier se décompose en une somme de nombres de Fibonacci, et de plus cette décomposition est unique si ces Fibonacci sont distincts, non consécutifs, et d'indices au moins 2.

Chargez la bibliothèque des listes:
```
Require Import List.
Import ListNotations.
```

 - Ecrire une fonction `fib_inv : nat -> nat` telle que si `fib_inv n = k` alors `fib k <= n < fib (k+1)`.

 - Ecrire une fonction `fib_sum : list nat -> nat` telle que `fib_sum [k_1;...;k_p] = fib k_1 + ... + fib k_p`.

 - Ecrire une fonction `decomp : nat -> list nat` telle que `fib_sum (decomp n) = n` et que `decomp n` soit sans 0, sans 1, sans doublons ni entiers consécutifs.

 - (Bonus) Ecrire une fonction `normalise : list nat -> list nat` qui part d'une décomposition sans 0 ni 1 ni doublons, mais peut-être avec des entiers consécutifs, et qui construit une décomposition sans 0 sans 1 sans doublons sans consécutifs, tout en préservant évidemment la somme totale. On pourra supposer que l'entrée est triée dans l'ordre croissant.
