Programmation Fonctionnelle en Coq : TD3
========================================

**M2 LMFI**

Rappel :

```coq
Require Import Bool Arith List.
Import ListNotations.
```

## Classiques sur les listes

Programmer les fonctions suivantes, sans utiliser les fonctions existantes de Coq :

 - `length`
 - concaténation (`app` en coq, notation infixe `++`)
 - retournement
 - `map : forall {A B}, (A->B)-> list A -> list B`
 - `filter : forall {A}, (A->bool) -> list A -> list A`
 - au moins un `fold`
 - `seq : nat -> nat -> list nat`, tel que `seq a n = [a; a+1; ... a+n-1]`

Quelles difficultés y-a-t'il à écrire des fonctions comme `head`, `last` ou encore `nth` ? Quelles solutions peut-on utiliser ?

## Quelques prédicats exécutables sur les listes

 - `forallb : forall {A}, (A->bool) -> list A -> bool`
 - `increasing` testant si une liste d'entiers est bien croissante strictement
 - `delta` testant si deux entiers consécutifs d'une liste sont bien toujours écarté d'au moins un certain `k`
 
## Mergesort

 - Ecrire une fonction `split` qui coupe une liste en deux parties de même taille (ou presque). Peu importe où un élément d'origine se retrouve à l'arrivée.
 
 - Ecrire une fonction `merge` qui fusionne deux listes triées en une liste triée. Ceci peut se faire de manière récursive structurelle à l'aide d'un `fix` interne.
 
 - Ecrire une fonction de tri `mergesort` utilisant les fonctions précédentes.

## Powerset 

Ecrire une fonction `powerset` qui à partir d'une liste `l` donne la liste de toutes les parties de `l`.
Par exemple `powerset [1;2] = [[];[1];[2];[1;2]]`. Peu importe l'ordre des éléments dans la liste produite.
