TD5 : Dependent types, monads, modules
======================================

**M2 LMFI**

#### Exercise 1 : prefect binary trees

Redo Exercise 2 of TD4, but this time use a dependent type for trees in `blist`, to ensure that all trees are necessarily perfect.

#### Exercise 2 : error monad

In the case of a (non-dependent) binary tree structure, use the error monad for writing a function `perfect_depth` that computes the depth of a tree while failing if this tree is not perfect.

#### Exercise 3 : list monad

By using a list monad, solve these little quizz :

 1. In a rugby game, when a team scores, its score gets increased by either 3, 5 or 7 points. Give all possible scores of a team that scored ten times. Same question for a team that scored at most ten times. How to modify the `bind` function to avoid redundant results in a list monad ?
 
 2. Generate all the pythagorean triples below 100, i.e. the triples of numbers `(a,b,c)` each between 1 and 100 and such that `a^2 + b^2 = c^2`.
 
 3. Laura, Romain, Bernard and Patricia are aligned for a photo. Patricia and Laura want to be next to each other and Romain want to be next to Laura. With these wishes, in how many ways could they align? (source: Kangourou CE2-CM1-CM2 2012).

#### Exercise 4 : State monad

Use a state monad to write a function taking a tree and returning another tree with the same shape, but where the data are the increasing numbers 0,1, .. n. You can choose trees where the data are on the leaves or on the nodes.

#### Exercice 5 : Modular interface for b-lists

Adapt TD4 by proposing a modular interface (with a parametrized type `t` and operations `cons` and `decons` and `nth`), and then check that your b-lists (or qb-lists) fulfill this interface.
