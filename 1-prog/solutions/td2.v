
Require Import Arith NArith List Bool.
Import ListNotations.

(** Exo 1 : cf td1.v *)

(** Exo 2 : Fibonacci *)

Fixpoint fib n :=
 match n with
 | 0 => 0
 | 1 => 1
 | S ((S n) as p) => fib p + fib n
 end.

Compute List.map fib (List.seq 0 20).

(** Un premier exemple de test de fonction
    via une specification executable *)

Definition fib_spec n := fib (n+2) =? fib n + fib (n+1).

Time Compute List.forallb fib_spec (List.seq 0 20).

Fixpoint fib2 n :=
  match n with
  | 0 => (0,1)
  | S n => let (a,b) := fib2 n in (b,a+b)
  end.

Definition fib_opt n := fst (fib2 n).

Compute List.map fib_opt (List.seq 0 20).

Fixpoint fibloop n a b :=
  match n with
  | 0 => a
  | S n => fibloop n b (a+b)
  end.

Definition fib_tailrec n := fibloop n 0 1.

Compute List.map fib_tailrec (List.seq 0 20).

(** En essayant de calculer des "gros" fibonacci on obtient
    "stack overflow" avec les trois versions précédentes.
    En fait c'est l'afficheur de [nat] qui ne suit plus !
    Une ruse possible est de traduire en [N] (binaire) avant
    affichage. *)

Time Compute (N.of_nat (fib 35)). (** une dizaine de secondes *)
Time Compute (N.of_nat (fib_opt 35)). (** 6s *)
Time Compute (N.of_nat (fib_tailrec 35)).  (** 4s *)

(** Mieux : autant faire les additions en binaire ! *)

Fixpoint fibN n :=
 match n with
 | 0 => 0%N
 | 1 => 1%N
 | S ((S n) as p) => (fibN p + fibN n)%N
 end.

Fixpoint fibN2 n :=
  match n with
  | 0 => (0,1)%N
  | S n => let (a,b) := fibN2 n in (b,a+b)%N
  end.

Definition fibN_opt n := fst (fibN2 n).

Fixpoint fibNloop n a b :=
  match n with
  | 0 => a
  | S n => fibNloop n b (a+b)%N
  end.

Definition fibN_tailrec n := fibNloop n 0%N 1%N.

Time Compute (fibN 35). (** 2s *)
Time Compute (fibN_opt 35). (** 0s *)
Time Compute (fibN_tailrec 35).  (** 0s *)

Time Compute fibN_tailrec (10*1000). (** 1s *)
Compute N.log2 (fibN_tailrec (10*1000)).
(** fib 10000 fait environ 7000 bits, soit 2000 chiffres décimaux *)

(** NB: le 10*1000 est pour éviter d'exploser le parser de nat,
    qui fait des "stack overflow" pour des constantes au dela
    de 5000. *)

(** Peut-on se passer complètement de [nat] ?
    Pas si simple alors de rester décroissant structurellement.
    On peut éventuellement utiliser [N.peano_rect]. *)

Check N.peano_rect.

(** Le mystère de comment faire un [N.peano_rect] ?
    Cf [Print Pos.peano_rect] et voir qu'il est bien decroissant structurel *)

Definition fibNN2 n :=
  N.peano_rect
    _
    (0,1)%N
    (fun n p => let '(a,b) := p in (b,a+b)%N)
    n.

Definition fibNN n := fst (fibNN2 n).

(** Note: L'argument [_] caché dans [fibNN2] est [(fun _ => (N*N)%type)].
    Ce qui indique qu'on utilise [N.peano_rect] de façon *non-dépdendante*
    (les résultats ont toujours le même type, indépendemment de n).
*)

Compute List.map fibNN [0;1;2;3;4;5;6;7;8;9;10]%N.

Time Compute fibNN 10000.

Definition fibNNloop n :=
 N.peano_rect
   _
   (fun a b => a)
   (fun _ p a b => p b (a+b)%N)
   n.

Definition fibNN_tail n := fibNNloop n 0%N 1%N.

Compute List.map fibNN [0;1;2;3;4;5;6;7;8;9;10]%N.

Time Compute fibNN_tail 10000.


(** Exo 3 : Fibonacci matriciel *)

Definition fibmat := (1,1,1,0)%N.

Definition multmat u v :=
  let '(u11,u12,u21,u22) := u in
  let '(v11,v12,v21,v22) := v in
  (u11*v11+u12*v21, u11*v12+u12*v22,
   u21*v11+u22*v21, u21*v12+u22*v22)%N.

Compute multmat fibmat fibmat.

Fixpoint powmat m p :=
  match p with
  | 1 => m
  | p~0 => let m' := powmat m p in multmat m' m'
  | p~1 => let m' := powmat m p in multmat m (multmat m' m')
  end%positive.

Definition fib_m p :=
  let '(_,a,_,_) := powmat fibmat p in a.

Compute List.map fib_m [1;2;3;4;5;6;7;8;9;10]%positive.

Time Compute fibNN (10000). (* 1s *)
Time Compute fib_m (10000). (* 2s *)


(** Exo 4 : Ecriture en base fibonacci *)

Fixpoint fib_inv_loop n a b k cpt :=
  match cpt with
  | 0 => 0
  | S cpt =>
    if n <? b then k
    else fib_inv_loop n b (a+b) (S k) cpt
  end.

Definition fib_inv n :=
 if n =? 0 then 0 else fib_inv_loop n 1 2 2 n.

Compute fib_inv 0.
Compute fib_inv 1.
Compute fib_inv 2.
Compute fib_inv 5.
Compute (fib 5, fib 6).
Compute fib_inv 10.
Compute (fib 6, fib 7).
Compute fib_inv 1000.
Compute (fib 16, fib 17).

(** Test plus systématique, via une spécification exécutable *)

Definition fib_inv_spec n :=
 (n =? 0) ||
 (let k := fib_inv n in
   (fib k <=? n) && (n <? fib (S k))).

Compute List.forallb fib_inv_spec (List.seq 0 1000).

Fixpoint decomp_loop n cpt :=
 match cpt with
 | 0 => []
 | S cpt =>
   if n =? 0 then []
   else
     let k := fib_inv n in
     k :: (decomp_loop (n-fib k) cpt)
 end.

Definition decomp n := decomp_loop n n.

Fixpoint sumfib l :=
  match l with
  | [] => 0
  | k::l => fib k + sumfib l
  end.

Compute decomp 10.
Compute sumfib [6;3].

Compute decomp 100.
Compute sumfib [11;6;4].

(** Specification exécutable de [decomp].
    C'est une spécification partielle : on ne vérifie pas que
    la liste produite est sans doublons ni entiers consécutifs *)

Definition decomp_spec n :=
 sumfib (decomp n) =? n.

Compute List.forallb decomp_spec (seq 0 1000).

(** Bonus : normalisation d'une décomposition sans doublons,
    déjà triée par l'ordre croissant. *)

Fixpoint normalise_loop l n :=
 match n with
 | 0 => l
 | S n =>
   match l with
   | [] => []
   | x::l =>
     match normalise_loop l n with
     | [] => [x]
     | y::l' =>
       if y =? x+1
       then normalise_loop ((y+1)::l') n
       else x::y::l'
     end
   end
 end.

Definition normalise l := normalise_loop l (length l).

Compute normalise [2;3;4;5;6].
Compute (sumfib [2;3;4;5;6], sumfib [2;5;7]).

Compute normalise [2;3;4;5;6;7].
Compute (sumfib [2;3;4;5;6;7], sumfib [4;6;8]).

(** Bonus : équation de la fonction qui décroit de 1 les décompositions *)

Definition g n := sumfib (List.map pred (decomp n)).

Definition g_spec n := (g n =? n - g (g (n-1))).

Compute List.forallb g_spec (seq 0 1000).
