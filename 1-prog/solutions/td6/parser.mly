%{
open Expr

let rec i2n = function 0 -> O | n -> S (i2n (n-1))
%}

%token <int> INT
%token PLUS TIMES
%token LPAREN RPAREN
%token EOL

%left PLUS        /* lowest precedence */
%left TIMES       /* medium precedence */

%start <Expr.expr> main

%%

main:
| e = expr EOL
    { e }

expr:
| i = INT
    { Num (i2n i) }
| LPAREN e = expr RPAREN
    { e }
| e1 = expr PLUS e2 = expr
    { Plus (e1,e2) }
| e1 = expr TIMES e2 = expr
    { Mult (e1,e2) }

