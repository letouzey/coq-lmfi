
type nat =
| O
| S of nat

type 'a list =
| Nil
| Cons of 'a * 'a list

(** val app : 'a1 list -> 'a1 list -> 'a1 list **)

let rec app l m =
  match l with
  | Nil -> m
  | Cons (a, l1) -> Cons (a, (app l1 m))

(** val add : nat -> nat -> nat **)

let rec add n m =
  match n with
  | O -> m
  | S p -> S (add p m)

(** val mul : nat -> nat -> nat **)

let rec mul n m =
  match n with
  | O -> O
  | S p -> add m (mul p m)

type expr =
| Num of nat
| Plus of expr * expr
| Mult of expr * expr

(** val eval : expr -> nat **)

let rec eval = function
| Num n -> n
| Plus (e0, e') -> add (eval e0) (eval e')
| Mult (e0, e') -> mul (eval e0) (eval e')

type inst =
| PUSH of nat
| ADD
| MUL

type prog = inst list

type stack = nat list

(** val exec_inst : inst -> stack -> stack **)

let exec_inst i stk =
  match i with
  | PUSH n -> Cons (n, stk)
  | ADD ->
    (match stk with
     | Nil -> Nil
     | Cons (e1, l) ->
       (match l with
        | Nil -> Nil
        | Cons (e2, stk') -> Cons ((add e1 e2), stk')))
  | MUL ->
    (match stk with
     | Nil -> Nil
     | Cons (e1, l) ->
       (match l with
        | Nil -> Nil
        | Cons (e2, stk') -> Cons ((mul e1 e2), stk')))

(** val exec_prog : prog -> stack -> stack **)

let rec exec_prog p stk =
  match p with
  | Nil -> stk
  | Cons (i, p') -> let stk' = exec_inst i stk in exec_prog p' stk'

(** val compile : expr -> prog **)

let rec compile = function
| Num n -> Cons ((PUSH n), Nil)
| Plus (e0, e') -> app (compile e0) (app (compile e') (Cons (ADD, Nil)))
| Mult (e0, e') -> app (compile e0) (app (compile e') (Cons (MUL, Nil)))
