
type nat =
| O
| S of nat

type 'a list =
| Nil
| Cons of 'a * 'a list

val app : 'a1 list -> 'a1 list -> 'a1 list

val add : nat -> nat -> nat

val mul : nat -> nat -> nat

type expr =
| Num of nat
| Plus of expr * expr
| Mult of expr * expr

val eval : expr -> nat

type inst =
| PUSH of nat
| ADD
| MUL

type prog = inst list

type stack = nat list

val exec_inst : inst -> stack -> stack

val exec_prog : prog -> stack -> stack

val compile : expr -> prog
