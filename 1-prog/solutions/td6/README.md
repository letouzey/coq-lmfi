TD6 : Little lexer/parser of arithmetical expressions
=====================================================

- `expr.ml` and `expr.mli` are obtained from `../td6.v` by extraction
- to compile, install `ocaml`, `dune` and `menhir` then do `make`
- to run : `make run`

Example of session:

```sh
$ make run
dune exec ./calc.exe
expr? 3*5+7        
eval: 22 compiled: 22 
```
