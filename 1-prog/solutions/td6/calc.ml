open Expr

let rec n2i = function O -> 0 | S n -> 1+n2i n

let process linebuf =
  Format.printf "expr? %!";
  try
    let e = Parser.main Lexer.token linebuf in
    let n = Expr.eval e in
    let n' =
      match Expr.exec_prog (Expr.compile e) Nil with
      | Cons (n,_) -> n
      | Nil -> failwith "empty stack"
    in
    Printf.printf "eval: %d compiled: %d \n%!" (n2i n) (n2i n')
  with
  | Failure msg ->
      Printf.fprintf stderr "%s%!" msg
  | Parser.Error ->
      Printf.fprintf stderr "At offset %d: syntax error.\n%!" (Lexing.lexeme_start linebuf)

let () =
  process (Lexing.from_channel stdin)

