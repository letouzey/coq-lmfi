Programmation Fonctionnelle en Coq (séance 2)
=============================================

**M2 LMFI**

## Contraintes d'univers

En complément de la séance 1 : Coq rejète certains usages des univers menant à des cycles de typage, qui mettent en danger la cohérence de la logique. La raison en est similaire au paradoxe de Russel, on parle ici de [paradoxe de Hurkens](https://coq.inria.fr/library/Coq.Logic.Hurkens.html) en théorie des types.

Voir [corrections/td1.v](corrections/td1.v) pour un exemple de *universe inconsistency* lors du `church_minus` (prolongement de l'exo 3): on voudrait que le type `church` puisse servir en tant que `X` dans le `forall X, (X->X)->(X->X)` qu'est `church` lui-même. Mais si `X` est dans un certain `Type_i`, alors `church` ne peut être qu'au niveau `Type_(i+1)` ou plus (faites ce typage vous-même!). Et donc on ne peut pas former l'application `church church` si les niveaux d'univers sont figés lors de la définition de `church`. Solution ici avec un Coq moderne : activer le *polymorphisme d'univers*, qui déterminera les niveaux d'univers de chaque instance de `church` à l'utilisation, et non à la définition. Cela aide grandement en pratique (mais pas toujours).

## Récursion générale et cohérence logique

Coq est cohérent tant qu'il est impossible de former une preuve *close* de `False`. Ici *close* signifie sans variables ni axiomes dans l'environnement. Sans même savoir comment ̀False` est défini, une récursivité générale nous permettrait d'avoir une telle preuve. Rappel: en Coq il n'y a pas de différence syntaxique entre une preuve et un programme.

```coq
Fixpoint loop (n:nat) : False := loop n
Definition boom : False := loop 0.
```

Evidemment une telle définition est rejeté par Coq. Voici l'équivalent en `OCaml` (ou il n'y a pas de question de cohérence logique):

```ocaml
let rec loop (n:int) : 'a = loop n
let any : 'a = loop 0 (* typage ok, par contre ça boucle à l'exécution *)
```

De même Coq repose de manière cruciale sur le fait qu'un terme clôt d'un type inductif va se réduire vers un des constructeurs de ce type, suivi du nombre d'arguments qui va bien. Ceci permet de montrer des propriétés du genre : tout booléen est égal à `true` ou bien à `false`, ou encore que tout entier `nat` est soit zéro, soit un successeur.

Là encore une récursivité générale briserait cette propriété. Par exemple:

```coq
Fixpoint flipflop (b:bool) := negb (flipflop b).
Definition alien : flipflop true.
```

Si `flipflop` était accepté, on aurait l'équation `flipflop true = negb (flipflop true)`, et donc `alien = negb alien`. Cet `alien` ne pourrait donc être ni `true` ni `false`.

## Types inductifs

En général, la syntaxe d'un type inductif est de la forme:

```coq
Inductive t :=
| C₁ : A₁₁ -> ... -> A₁ₚ -> t
| ...
| Cₙ : Aₙ₁ -> ... -> Aₙₖ -> t
```

Les `Cᵢ` sont les *constructeurs* du type `t`, ils peuvent attendre des arguments (ou pas), mais ont toujours `t` comme type de sortie.

En fait, `t` lui-même peut avoir des arguments, ce qui en fait un *schéma de type inductif*, on parle aussi de *prédicat inductif*. On verra des exemples par la suite.

## Condition de positivité

Certains inductifs ne sont pas acceptés par Coq, il s'agit encore d'une contrainte lié à la cohérence logique. Grosso modo, le type de l'inductif en cours de définition ne peut pas apparaître en argument d'un argument d'un constructeur de ce type. Cette condition est nommé *stricte positivité*.

Illustration du risque, en OCaml:

```ocaml
(* Primo, version "lambda-calcul" *)
type lam = Fun : (lam -> lam) -> lam
let identity = Fun (fun t -> t)
let app (Fun f) g = f g
let delta = Fun (fun x -> app x x)
let dd = app delta delta (* evaluation infinie, sans let rec ! *)

(* Secondo, une version produisant du 'a (donc possiblement False en Coq) *)
type 'a poly = Poly of ('a poly -> 'a)
let app (Poly f) g = f g
let delta = Poly (fun x -> app x x)
let dd : 'a = app delta delta
```

## Match

Le *match* (ou *pattern-matching*) est une analyse de cas, en suivant les constructeurs d'un type inductif.
A part de petites différences syntaxique (`=>` et mot-clé `end`), il se comporte de manière très similaire au `match` d'OCaml.

```coq
match ... with
| C₁ x₁₁ ... x₁ₚ => ...
| ...
| Cₙ xₙ₁ ... xₙₖ => ...
end
```

La *tête* du match (ce qu'il y a entre `match` et `with`) doit être du bon type inductif, celui correspondant aux constructeurs `C₁` ... `Cₙ`.

Habituellement, les *branches* (les portions après `=>`) contiennent du code d'un même type. On verra par la suite que ceci n'est pas obligatoire (cf. la séance sur les types *dépendants*).

Réduction d'un match : lorsque la tête d'un match commençe par un constructeur inductif `Ci`, une *iota-réduction* est possible, elle remplace alors tout le match par la branche correspondante au constructeur `Ci`, en replaçant dedans les variables `xi₁`...`xiₚ` par les arguments concrets du constructeur en tête de match.

## Fix

La construction `Fixpoint` va permettre de créer des fonctions récursives en Coq. Attention, comme mentionné auparavant, certaines fonctions récursives seront rejetées par Coq, qui ne va accepter que des *fonctions récursives structurellement décroissantes*.

Le mot-clé `Fixpoint` s'emploie à la place du mot-clé `Definition`, voir les exemples plus bas (ou dans les TD).

Il est également possible de définir une fonction récursive interne, à tout endroit d'un code, via le mot-clé `fix` (exemples la semaine prochaine). En fait, `Fixpoint` n'est pas une construction primitive de Coq, c'est un `Definition` suivi d'un `fix`.

Un `Fixpoint` ou `fix` définit obligatoirement une fonction, un argument de cette fonction joue un rôle particulier, on parle d'argument de décroissance (ou de *garde*). Avant d'accepter cette fonction récursive, Coq vérifie que chaque appel récursif se fait sur un sous-terme strict de cet argument (c'est-à-dire quelque-chose sortant d'un match sur cet argument). Par défaut, Coq cherche automatiquement quelque argument peut jouer ce rôle de garde, mais on peut le spécifier manuellement (syntaxe `{struct n}`).

Réduction d'un `Fixpoint` ou `fix` : lorsque l'argument de garde d'un fix commençe par un constructeur inductif `Ci`, une réduction est possible (on parle aussi de *iota-réduction*). On remplace alors tout le fix par son corps (ce qui suit le `:=`), en changeant dedans le nom de la fonction récursive par le fix de départ (pour permettre des itérations ultérieures).

## Quelques types inductifs usuels

### Les entiers naturels en représentation de Peano

```coq
Print nat.
```

### Représentation binaire des entiers

```coq
Require Import ZArith.
Print N.
Print positive.
Print Z.
```

Note : passer par un type des entiers strictement positifs permet d'assurer l'unicité de la représentation, aussi bien pour `N` que pour `Z`. En particulier il y a un seul codage de zéro (`N0` dans le type `N`, `Z0` dans le type `Z`).

### Paires Coq

```coq
Print prod.

Definition fst {A B} (p:A*B) := match p with
 | (a,b) => a
 end.

Definition fst' {A B} (p:A*B) :=
 let '(a,b) := p in a.
```

### Un premier exemple de type dépendant

```coq
Inductive unit : Type := tt : unit.

Fixpoint pow n : Type :=
 match n with
 | 0 => unit
 | S n => (nat * (pow n))%type
 end.
```

### Le type option

```coq
Print option.
```

### Le type des listes

```coq
Print list.

Require Import List.
Import ListNotations.

Check (3 :: 4 :: []).

Fixpoint length {A} (l : list A) :=
 match l with
 | [] => 0
 | x :: l => S (length l)
 end.
```

### Arbres en Coq

Contrairement à ce qui précède, ceci n'est pas un type prédéfini en Coq. En effet il y a tellement de types d'arbres différents selon les besoins que c'est à l'utilisateur de définir le sien. Par exemple, voici une version avec rien aux feuilles et une donnée entière aux noeuds.

```ocaml
Inductive tree :=
| leaf
| node : nat -> tree -> tree -> tree.
```
