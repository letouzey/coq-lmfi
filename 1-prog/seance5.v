
Require Import List Arith.
Import ListNotations.

(** * Types dépendants, suite *)

(** Arbres binaires complets, via types inductifs *)

Inductive fulltree (A:Type) : nat -> Type :=
| FLeaf : A -> fulltree A 0
| FNode n : fulltree A n -> fulltree A n -> fulltree A (S n).

Arguments FLeaf {A}.
Arguments FNode {A} {n}.

Check FNode (FNode (FLeaf 1) (FLeaf 2)) (FNode (FLeaf 1) (FLeaf 2)).



(** Paires dépendantes *)

Print sigT.

Definition blist (A:Type) := list {n:nat & fulltree A n}.

Definition singleton {A} (a:A) : blist A := [ existT _ 0 (FLeaf a) ].


(** Le type [Fin] *)

Inductive Fin : nat -> Type :=
 | Zero n : Fin (S n)
 | Succ n : Fin n -> Fin (S n).

Arguments Zero {n}.
Arguments Succ {n}.

Check (Zero : Fin 1).
Fail Check (Succ Zero : Fin 1).

Check (Zero : Fin 2).
Check (Succ Zero : Fin 2).


(** Application: [Vnth]

    Les entiers "bornées" du type Fin permettent de donner un
    type précis et un comportement sans erreur à l'accès
    au "n-ieme" élément d'un vecteur. *)

(** On reprend le type [vect] de la semaine dernière: *)

Inductive vect (A:Type) : nat -> Type :=
 | Vnil : vect A 0
 | Vcons n : A -> vect A n -> vect A (S n).

Arguments Vnil {A}.
Arguments Vcons {A} {n}.

Fixpoint Vnth {A} {n} (p:Fin n) : vect A n -> A :=
 match p with
 | Zero => fun v => match v with Vcons x _ => x end
 | Succ p => fun v => Vnth p (match v with Vcons _ v => v end)
 end.

(** NB: ce genre de programmation est encore relativement nouveau
    en Coq, et encore fort délicat. Par exemple, l'exemple précédent
    est rejeté par Coq si on déplace l'appel à [Vnth] sous
    le dernier match, ou qu'on rassemble les deux [fun v =>]
    en un seul avant le [match p].
*)

(** Exemples d'utilisation : avec une liste de taille 3,
    on peut bien accéder aux éléments d'indice 0, 1, 2, mais
    pas 3. *)

Definition testvec := Vcons 1 (Vcons 2 (Vcons 3 Vnil)).

Compute Vnth Zero testvec.
Compute Vnth (Succ Zero) testvec.
Compute Vnth (Succ (Succ Zero)) testvec.
Fail Compute Vnth (Succ (Succ (Succ Zero))) testvec.



(** * Monades *)

Definition head {A} (l:list A) : option A :=
 match l with
 | [] => None
 | x :: _ => Some x
 end.

Definition tail {A} (l:list A) : option (list A) :=
 match l with
 | [] => None
 | _ :: l => Some l
 end.

(** Exemple: la somme des deux premiers entiers d'une liste.
    Evidemment, ça peut se faire de manière directe. *)

Definition somme_deux_premiers l :=
 match l with
 | a::b::_ => Some (a+b)
 | _ => None
 end.

(** Mais si maintenant on veut faire ça via des [head] et [tail],
    c'est très lourd: *)

Definition somme_deux_premiers2 l :=
 match head l with
 | None => None
 | Some a =>
   match tail l with
   | None => None
   | Some l' =>
     match head l' with
     | Some b => Some (a+b)
     | _ => None
     end
   end
 end.

(** Un peu d'abstraction, via une fonction d'ordre supérieur *)

(** Premier essai: *)

Definition option_map {A B} (o : option A)(f:A -> B) : option B :=
 match o with
 | Some x => Some (f x)
 | None => None
 end.

Definition somme_deux_premiers3 l :=
 option_map (head l)
   (fun a => option_map (tail l)
      (fun l' => option_map (head l')
         (fun b => a+b))).

Check somme_deux_premiers3. (* Pas le bon type !!! *)

(** Mieux: *)

Definition option_bind {A B} (o : option A) (f:A -> option B) : option B :=
 match o with
 | Some x => f x
 | None => None
 end.

Definition somme_deux_premiers4 l :=
 option_bind (head l)
   (fun a => option_bind (tail l)
      (fun l' => option_bind (head l')
         (fun b => Some (a+b)))).

Check somme_deux_premiers4.

(** Avec une jolie notation *)

Infix ">>=" := option_bind (at level 20, left associativity).

Definition somme_deux_premiers5 l :=
 head l >>= fun a =>
 tail l >>= fun l' =>
 head l' >>= fun b =>
 Some (a+b).

Compute somme_deux_premiers5 [1;2;3].

(** Généralisation : l'interface des monades *)

Module Type MONAD.
  Parameter t : Type -> Type.
  Parameter ret : forall {A}, A -> t A.
  Parameter bind : forall {A B}, t A -> (A -> t B) -> t B.
End MONAD.

(** Les lois monadiques (qu'on garde ici implicites):

return a >>= f   =  f a
m >>= return     =  m
(m >>= f) >>= g  =  m >>= (fun x -> (f x >>= g))

*)

(** La monade d'erreur *)

Module MErr <: MONAD.
  Definition t := option.
  Definition ret {A} := @Some A.
  Definition bind {A B} := @option_bind A B.
End MErr.

Infix ">>=" := MErr.bind (at level 20, left associativity).

Definition somme_deux_premiers6 l :=
 head l >>= fun a =>
 tail l >>= fun l' =>
 head l' >>= fun b =>
 MErr.ret (a+b).

Inductive tree (A:Type) :=
| Leaf : A -> tree A
| Node : tree A -> tree A -> tree A.

Arguments Leaf {A}.
Arguments Node {A}.

(** Exemple, multiplication des feuilles d'un arbre,
    avec arrêt au premier zero *)

Fixpoint leafmul t :=
  match t with
  | Leaf n =>
    if n =? 0 then None
    else Some n
  | Node g d =>
    leafmul g >>= fun mulg =>
    leafmul d >>= fun muld =>
    Some (mulg * muld)
  end.

Definition assert (b:bool) : option unit :=
 if b then Some tt else None.

Fixpoint leafmul' t :=
  match t with
  | Leaf n =>
    assert (negb (n =? 0)) >>= fun _ => Some n
  | Node g d =>
    leafmul' g >>= fun mulg =>
    leafmul' d >>= fun muld =>
    Some (mulg * muld)
  end.


(** ** Monade de liste *)

Module MList <: MONAD.
  Definition t := list.
  Definition ret {A} (a:A) := [a].
  Definition bind {A B} (l:list A) (f:A->list B) := List.flat_map f l.
End MList.

Infix ">>=" := MList.bind (at level 20, left associativity).

(** Exemple, produit cartésien *)

Definition cartprod {A} {B} (l:list A)(l' : list B) :=
  l >>= fun a =>
  l' >>= fun b =>
  MList.ret (a,b).

Compute cartprod [1;2;3] [4;5].

Definition filter {A} (f : A -> bool) : list A -> list A := List.filter f.


(** ** Monade d'état *)
Module MState <: MONAD.
  Definition t A := nat -> nat*A.
  Definition ret {A} (a:A) := fun (st:nat) => (st,a).
  Definition bind {A B} (m:t A) (f:A->t B) :=
   fun st => let (st',a) := m st in f a st'.
End MState.
