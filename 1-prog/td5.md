TD5 : Types dépendants, monades, modules
========================================

**M2 LMFI**

#### Exercice 1 : Arbres binaires complets

Reprendre le TD4, mais cette fois-ci utiliser un type dépendant pour représenter les arbres binaires complets.

#### Exercice 2 : Monade d'erreur

Dans le cas d'une structure d'arbre binaire non-dépendante, utiliser la monade d'erreur pour écrire une fonction `hauteur_complet` qui donne la hauteur d'un arbre tout en échouant si cet arbre n'est pas complet.

#### Exercice 3 : Monade d'état

Utiliser la monade d'état pour écrire une fonction prenant un arbre et produisant un arbre de même squelette, mais où les étiquettes (aux feuilles ou aux noeuds, à votre choix) sont maintenant les entiers successifs.

#### Exercice 4 : Monade de liste

En utilisant une monade de liste, résoudre les problème suivants.

 1. Au rugby, lorsqu'une équipe marque, elle gagne 3, 5 ou 7 points. Donner les scores possible d'une équipe qui a marqué dix fois. Même question pour une équipe qui a marqué au plus dix fois. Comment modifier la fonction `bind` pour que les réponses ne contiennent pas de redondances ?
 
 2. Générer tous les triplets pythagoriciens inférieurs à 100, c'est-à-dire les triplets d'entiers `(a,b,c)` chacuns entre 1 et 100 et tels que `a^2 + b^2 = c^2`.
 
 3. Laura, Romain, Bernard et Patricia s'alignent pour une photo. Patricia et Laura veulent être à côté l'une de l'autre et Romain veut être à côté de Laura. En respectant tous ces voeux, de combien de manière peuvent-ils s'aligner tous les quatre pour la photo? (source: Kangourou CE2-CM1-CM2 2012).

#### Exercice 5 : interface modulaire pour les b-listes

Reprendre le TD4, en proposant maintenant une interface modulaire pour les b-listes (ou les qb-listes).
