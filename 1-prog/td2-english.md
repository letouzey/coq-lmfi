Functional Programming in Coq : TD2
===================================

**M2 LMFI**

#### Exercise 1 : usual functions on natural numbers

Load first the unary arithmetics : `Require Import Arith`.

If not done last week, define the following functions on `nat` (without using the ones of Coq standard library):

  - addition
  - multiplication
  - subtraction
  - factorial
  - power
  - gcd

#### Exercise 2 : Fibonacci

 - Define a function `fib` such that `fib 0 = 0`, `fib 1 = 1` then `fib (n+2) = fib (n+1) + fib n`.
   (you may use a `as` keyword to name some subpart of the `match` pattern ("motif" en français)).

 - Define an optimized version of `fib` that computes faster that the previous one by using Coq pairs.

 - Same question with just natural numbers, no pairs. Hint: use a special recursive style called "tail recursion".

 - Load the library of binary numbers via `Require Import NArith`.
   Adapt you previous functions for them now to have type `nat -> N`.
   What impact does it have on efficiency ?
   Is it possible to simply obtain functions of type `N -> N` ?
   
#### Exercise 3 : Fibonacci though matrices

 - Define a type of 2x2 matrices of numbers (for instance via a quadruple).
 - Define the multiplication and the power of these matrices.
   Hint: the power may use an argument of type `positive`.
 - Define a fibonacci function through power of the following matrix:

```
1 1
1 0
```

#### Exercise 4 : Fibonacci decomposition of numbers

We aim here at programming the Zeckendorf theorem in practice : every number can be decomposed in a sum of Fibonacci numbers, and moreover this decomposition is unique as soon as these Fibonacci numbers are distinct and non-successive and with index at least 2.

Load the list library:
```
Require Import List.
Import ListNotations.
```

 - Write a function `fib_inv : nat -> nat` such that if `fib_inv n = k` then `fib k <= n < fib (k+1)`.

 - Write a function `fib_sum : list nat -> nat` such that `fib_sum [k_1;...;k_p] = fib k_1 + ... + fib k_p`.

 - Write a function `decomp : nat -> list nat` such that `fib_sum (decomp n) = n` and `decomp n` does not contain 0 nor 1 nor any redundancy nor any successive numbers.

 - (Optional) Write a function `normalise : list nat -> list nat` which receives a decomposition without 0 nor 1 nor redundancy, but may contains successive numbers, and builds a decomposition without 0 nor 1 nor redundancy nor successive numbers. You might assume here that the input list of this function is sorted in the way you prefer. 

