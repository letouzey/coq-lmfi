Fonction Puissance en Arithmétique de Peano
===========================================

Projet Coq, M2 LFMI

## Remarque préliminaire

Ce projet Coq ne comporte pas de fichier Coq prêt à compléter, pas de subdivision préalable en suite de lemmes auxiliaires déjà énoncés. 
De même on trouvera un certain nombre d'indications ci-dessous, mais pas
autant que dans d'autres sujets. Il s'agit donc d'un intermédiaire en ces autres projets très cadrés et des travaux entièrement personnels, pour commencer à découvrir comment organiser un développement Coq tout en gardant
un fil directeur fourni. Ne pas hésiter à me contacter en cas de doute ou de détails manquants.


## Objectif

Le coeur du sujet de ce projet tient en quelques lignes : écrire un prédicat Coq `IsNthPowerOf : nat -> nat -> nat -> Prop` en utilisant uniquement le langage de l'arithmétique de Peano, puis prouver en Coq que
`forall x y z, IsNthPowerOf x y z <-> x = y^z`.

On rappelle que le langage de l'arithmétique de Peano est un sous-ensemble du langage de Coq, il s'agit de `O` `S` `+` `*`, de l'égalité `=` (sur des entiers), des connecteurs logiques `/\` `\/` `->` `~` `<->` et les quantifications `forall` et `exists` tant qu'on quantifie uniquement sur des entiers. Et c'est tout...

Il est évidemment possible (et même recommandé) de définir `IsNthPowerOf` en plusieurs prédicats intermédiaires, tant que chacun de ces fragments reste dans le langage demandé. Par exemple on pourra commencer par :

```coq
Definition Le x y := exists z, x+z = y.
```

Puis faire une preuve d'équivalence entre ce `Le` et l'ordre `<=` standard de Coq sur les `nat`, défini lui via un `Inductive` (Cf. la fin de la feuille de TP 2.2 à ce propos).


## Bibliothèques utilisables

Ce projet peut utiliser toute la bibliothèque standard de Coq, version de votre choix. Mais pas d'autres bibliothèques "externes" autorisées ici. En particulier il est recommandé de charger au minimum :

```coq
Require Import Arith Lia
```

Sans cela, la notation `^` pour la fonction puissance `Nat.pow` de Coq ne sera même pas disponible. Une fois cette bibliothèque `Arith` chargée, voir par exemple `Search "^"` pour lister tous les lemmes disponibles concernant la fonction puissance de Coq. La tactique `lia` pourra aussi être pratique, elle permet de résoudre des ensembles d'(in)égalités de l'arithmétique de Presburger (additions et soustractions quelconques, multiplications par des constantes).

Vous aurez sans doute également besoin d'un peu de théorie des nombres : divisibilité, primalité. Voir en particulier `Nat.divide` et `Nat.gcd` et les lemmes les concernant. A priori, pas de définition de primalité sur `nat` dans la stdlib Coq, vous pouvez définir la vôtre et quelques résultats de base, ou vous appuyer sur cette qui existe pour les nombres relatifs `Z` : voir les bibliothèques `ZArith` et `Znumtheory`, et passer par des fonctions de conversions `Z.of_nat` et `Z.to_nat`. Pas non plus de choses comme le théorème des restes chinois (ni sur `nat` ni sur `Z` cette fois-ci), si vous en avez besoin il faudra en démontrer une version, cela fait partie de l'exercice.

## Indications

Pas d'inquiétude si vous ne trouvez pas seul une implémentation possible de `IsNthPowerOf`. Tout le monde ne s'appelle pas Kurt Gödel ! Voici deux approches possibles. Votre projet Coq sera validé si une méthode au moins
est définie et prouvée en Coq, peu importe la source d'inspiration. Vous pouvez même implémenter les deux, histoire de les comparer.

Si par contre vous souhaitez chercher encore par vous-même, attention aux "spoilers" ci-dessous (ou "divulgâchage" comme on dit au canada).

Déjà, première remarque, il y a en fait deux problèmes très voisins qui sont parfois confondus : ce `IsNthPowerOf` demandé ci-dessus, et un prédicat
plus faible consistant à exprimer que `x` est une puissance de `y`, peu importe laquelle. Autrement dit, écrire un `IsPowerOf : nat -> nat -> Prop` tel que `forall x y, IsPowerOf x y <-> exists z, x = y^z`. Evidemment, on peut retrouver `IsPowerOf` à partir de `IsNthPowerOf` grâce à un simple `exists`. Par contre pas de réciproque évidente. Ceci dit, implémenter un `IsPowerOf` et le prouver peut être un bon entraînement en vu de l'autre prédicat.

Autre remarque, dans le cas particulier où `y` est un nombre premier, il s'avère assez simple d'écrire un `IsPowerOf x y` spécialisé à ce cas,
en étudiant qui peut diviser cette puissance `x` d'un nombre premier `y`.
Pas de généralisation simple à un `y` composé (10 par exemple dans le quiz
historique). Par contre ce prédicat dédié `IsPrimePower` sera utile 
pour la première méthode ci-dessous.

#### Première méthode : coder une suite finie dans des chiffres en base première

En prenant un nombre premier `e` assez grand, on peut encoder une suite entière finie `u_0` ...`u_n` en un seul nombre `u_0 + u_1 * e + ... u_n * e^n` dont les chiffres en base `e` sont alors les `u_i`. On accède à ces chiffres via des divisions par des puissances de `e`, puis des modulo `e`. Ici la suite à encoder ainsi est la suite des puissances de `y` à savoir `1`, `y`, `y^2`, jusqu'à `y^z` qui doit être `x`.

Pour les détails, je renvoie au lien suivant (dans lequel les notations `Ax:` et `Ex:` signifient respectivement `forall x,` et `exists x,`) :

https://stason.org/TULARC/self-growth/puzzles/331-logic-hofstadter-p.html

Attention, il peut rester des petits soucis dans ce texte, par exemple la définition utilisée pour `PRIME` mériterait d'exclure les cas 0 et 1. Et il y a moyen de simplifier encore la formule obtenue : par exemple l'usage de `LENGTH` ne semble pas indispensable. Noter que cela donne une solution pour le prédicat `IsPowerOf` pour `y=10` uniquement, mais la généralisation à `y` quelconque est immédiate. Passer ensuite à une solution de `IsNthPowerOf` peut nécessiter d'encoder deux suites à la fois : les puissances de `y` et les nombres successifs `1`, `2`, ...`z`. Dans tous les cas il faudra pouvoir montrer qu'il existe des nombres premiers aussi grands que l'on veut (théorème d'Euclide, qui ne semble être dans la stdlib Coq). 


#### Deuxième méthode : usage du théorème des restes chinois

Une autre approche possible est d'utiliser le théorème des restes chinois. Voir la seconde moitié du lien suivant (dans lequel les notations `Ax:` et `Ex:` signifient respectivement `forall x,` et `exists x,`) :

https://www.math.uni-bielefeld.de/~sillke/PUZZLES/power10

En particulier la toute fin propose une implémentation possible de `IsNthPowerOf`. Là encore il se peut que de petits ajustements soient nécessaires ici ou là, mais l'idée générale est la bonne. Par exemple lire peut-être plutôt `Aj: (0 < j <= w -> ...` à l'avant-dernière ligne. 
Et dans le long message central, les lignes commençant par `:` correspondent à une variante où la constante `d` est un peu moins gigantesque. Cette variante est intéressante mais complique la lecture, donc ignorer ces lignes dans un premier temps.


## Extensions possibles (Facultatives)

#### Écriture d'autres fonctions récursives en arithmétique de Peano

En utilisant les idées précédentes, il est possible de représenter en arithmétique de Peano tout un ensemble de fonctions récursives classiques.
Coder par exemple une relation correspondant à la fonction factorielle, et la prouver correcte. Note : cela peut nécessiter d'utiliser votre relation `IsNthPowerOf` fraîchement prouvée.


#### Lien avec le projet "Cohérence de Heyting"

Le langage de Coq étant nettement plus vaste que celui de Peano, comment peut-on être sûr d'avoir bien défini le prédicat `IsNthPowerOf` dans le bon langage, autrement que par une relecture de son code ?

Parmi les autres projets proposés cette année, l'un propose d'étudier la cohérence de l'arithmétique de Heyting (i.e. Peano sans logique classique) relativement à la cohérence de Coq. Pour cela, un encodage Coq de l'arithmétique de Heyting est proposé. On parle alors d'encodage profond ("deep embedding") car termes et formules sont encodés via des structures de données dédiées en Coq (via des `Inductive`). Définir votre prédicat `IsNthPowerOf` comme une formule `F` du type `formula` dans ce fichier `Heyting.v`. Cette formule devra avoir trois variables libres `Tvar 0` `Tvar 1` et `Tvar 2` correspondant respectivement à `x` `y` et `z`. Vérifier que `fun x y z => finterp [x;y;z] F` redonne bien un prédicat convertible à votre `IsNthPowerOf`.

