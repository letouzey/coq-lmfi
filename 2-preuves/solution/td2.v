
(* Exercise 1 *)

Lemma add_0_l : forall n, 0 + n = n.
Proof.
 intro. (* optional step *)
 simpl. (* optional step *)
 reflexivity.
Qed.

Lemma add_succ_l : forall n m, S n + m = S (n + m).
Proof.
 intros.
 simpl.
 reflexivity.
Qed.

Lemma add_0_r : forall n, n + 0 = n.
Proof.
 simpl. (* nothing, internally n+... = ... match n with ... *)
 induction n.
 - simpl. reflexivity.
 - simpl. f_equal. (* back to the initial statement, for n-1. *)
   (* with induction n instead of destruct n,
      now we have an extra IHn induction hypothesis *)
   apply IHn.
Qed.

Lemma add_succ_r : forall n m, n + S m = S (n + m).
Proof.
 induction n; simpl. (* induction and then simpl on both subgoals *)
 - reflexivity.
 - intros. f_equal. apply IHn. (* or "rewrite IHn" instead of the f_equal. *)
Qed.

Lemma add_assoc : forall n m p, (n + m) + p = n + (m + p).
Proof.
 induction n; simpl. (* avoid induction on m or p, that would not help
                        (less computations triggered). *)
 - reflexivity.
 - intros. f_equal. apply IHn.
Qed.

Lemma add_comm : forall n m, n + m = m + n.
Proof.
 induction m; simpl. (* For once, induction m is ok (symmetrical statement)
                        Compared with induction n, it even leads to
                        sub-equations in the right direction. *)
 - apply add_0_r.
 - rewrite add_succ_r. f_equal. apply IHm.
Qed.

Lemma add_comm' : forall n m, n + m = m + n.
Proof.
 (* rewrite ?foo will rewrite equation foo as more as possible, possibly
    0 times *)
 induction n; simpl; intros; rewrite ?add_0_r, ?add_succ_r, ?IHn; reflexivity.
Qed.


(* Exercise 2 *)

Lemma mul_0_l : forall n, 0 * n = 0.
Proof.
 reflexivity. (* definitional equality *)
Qed.

Lemma mul_succ_l : forall n m, S n * m = m + n * m.
Proof.
 reflexivity. (* same *)
Qed.

Lemma mul_0_r : forall n, n * 0 = 0.
Proof.
 induction n; simpl.
 - reflexivity.
 - apply IHn.
Qed.  (* could be shortened in induction n; simpl; trivial. *)

Require Import Setoid.

Lemma mul_succ_r : forall n m, n * S m = n + n * m.
Proof.
 induction n; simpl; intros.
 - reflexivity.
 - f_equal. rewrite IHn. rewrite <- 2 add_assoc. (* reverse rewrite two times *)
(* To answer a question by Enrique :  rewrite (add_comm n)
    for controling where add_comm is used. *)
   f_equal. apply add_comm.
Qed.

Lemma mul_distr : forall n m p, (n + m) * p = n * p + m * p.
Proof.
 induction n; simpl; intros.
 - reflexivity.
 - rewrite add_assoc. f_equal. apply IHn.
Qed.

(* Just to illustrate a bit of the tactic language, we can define
   out own tactics (here an alias for a longer sequence of tactics *)
Ltac induct n := induction n; simpl; intros.

Lemma mul_assoc : forall n m p, (n * m) * p = n * (m * p).
Proof.
 induct n. (* Same as induction n; simpl; intros. *)
 - reflexivity.
 - rewrite mul_distr. f_equal. apply IHn.
Qed.

Lemma mul_comm : forall n m, n * m = m * n.
Proof.
 induction m; simpl. (* just as for add, induction n is also ok here *)
 - apply mul_0_r.
 - rewrite mul_succ_r. f_equal. apply IHm.
Qed.

(* Exercise 3 *)

Definition le (n m : nat) := exists p, n + p = m.
Infix "<=" := le : alt_le_scope.
Open Scope alt_le_scope.

Lemma le_refl : forall n, n <= n.
Proof.
 unfold le. (* not mandatory, but convenient to understand what's going on*)
 intros. exists 0. apply add_0_r.
Qed.

Lemma le_trans : forall n m p, n <= m -> m <= p -> n <= p.
Proof.
 unfold le.
 intros n m p (q,Hq) (r,Hr). exists (q+r).
 rewrite <- add_assoc. rewrite Hq, Hr. reflexivity.
Qed.

Lemma add_more_0 : forall n m, n+m = n -> m = 0.
Proof.
 induction n; simpl; intros. (* ; auto finishes directly :-) *)
 - assumption.
 - injection H. apply IHn.
Qed.

Lemma add_0_left_0 : forall n m, n+m = 0 -> n = 0.
 induction n; simpl; intros. (* ; easy finishes directly :-) *)
 - reflexivity.
 - discriminate.
Qed.

Lemma le_antisym : forall n m, n <= m -> m <= n -> n = m.
Proof.
 unfold le.
 intros n m (q,Hq) (r,Hr). rewrite <- Hq in Hr.
 rewrite add_assoc in Hr.
 assert (q+r = 0).
 { apply (add_more_0 n). assumption. }
 assert (q = 0).
 { apply (add_0_left_0 q r). assumption. }
 rewrite H0 in Hq. rewrite add_0_r in Hq. assumption.
Qed.

(* another style : apply ... in ... *)

Lemma le_antisym' : forall n m, n <= m -> m <= n -> n = m.
Proof.
 unfold le.
 intros n m (q,Hq) (r,Hr). rewrite <- Hq in Hr.
 rewrite add_assoc in Hr.
 apply add_more_0 in Hr.
 apply add_0_left_0 in Hr.
 rewrite Hr in Hq. rewrite add_0_r in Hq. assumption.
Qed.


(* Bonus proofs : *)

Lemma le_succ : forall n m, n <= m -> n <= S m.
Proof.
 intros n m (q,H). exists (S q). rewrite add_succ_r. f_equal. apply H.
Qed.

Lemma le_total : forall n m, n <= m \/ m <= n.
Proof.
induction n.
- left; exists m; auto.
- destruct m.
  + right; exists (S n); auto.
  + destruct (IHn m).
    * left. destruct H as (q,H). exists q. simpl. f_equal. assumption.
    * right. destruct H as (q,H). exists q. simpl. f_equal. assumption.
Qed.

Close Scope alt_le_scope.
Locate "<=". (* Now <= is back to the default definition of Coq *)

Print Peano.le. (* The one of Coq : an inductive definition *)

Lemma coq_le_add_r : forall n m, n <= n+m.
Proof.
 induction m.
 - rewrite add_0_r. apply Peano.le_n.
 - rewrite add_succ_r. apply Peano.le_S. assumption.
Qed.

Lemma le_equiv : forall n m, n <= m <-> le n m.
Proof.
 split.
 - intros H; induction H. (* induction on a inductive proof ! *)
   + apply le_refl.
   + apply le_succ; assumption.
 - intros (q,H). rewrite <- H. apply coq_le_add_r.
Qed.
