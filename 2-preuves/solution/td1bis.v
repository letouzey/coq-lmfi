Module Exercise1.

Parameter E : Type.
Parameter R : E -> E -> Prop.
Axiom refl : forall x : E, R x x.
Axiom trans : forall x y z : E, R x y -> R y z -> R x z.
Axiom antisym : forall x y : E, R x y -> R y x -> x = y.

Definition smallest (x0 : E) :=
 forall x : E, R x0 x.
Definition minimal (x0 : E) :=
 forall x : E, R x x0 -> x = x0.

Lemma E1Q1 :
 forall x x', smallest x -> smallest x' -> x=x'.
Proof.
 intros x x' Hx Hx'.
 unfold smallest in *.
 apply antisym.
 apply Hx.
 apply Hx'.
Qed.

Lemma E1Q2 :
 forall x, smallest x -> minimal x.
Proof.
 intros x Hx.
 unfold smallest in *. unfold minimal.
 intros x' Hx'.
 apply antisym.
 apply Hx'.
 apply Hx.
Qed.

Lemma E1Q3 :
 forall x x', smallest x -> minimal x' -> x = x'.
Proof.
 intros x x' Hx Hx'.
 unfold smallest in *. unfold minimal in *.
 apply Hx'.
 apply Hx.
Qed.

End Exercise1.

Module Exercise2. (* Drinker lemma *)

(* Classical logic here: *)
Axiom not_not_elim : forall A : Prop, ~~A -> A.

Lemma EM : forall A, A\/~A.
Proof.
 intros A.
 apply not_not_elim.
 intro H. apply H. right.
 intro. apply H. left. assumption.
Qed.

Parameter E : Type. (* All the persons in the room *)
Parameter P : E -> Prop.
  (* Predicate telling if somebody drinks *)
Parameter e0 : E. (* the name of a person in the room *)

Lemma notexistsnot : ~(exists e, ~P e) <-> forall e, P e.
Proof.
 split.
 - intros H e.
   apply not_not_elim. intro. apply H. exists e. assumption.
 - intros H (e,H'). apply H'. apply H.
Qed.

Lemma DrinkerLemma : exists e, P e -> (forall y, P y).
Proof.
destruct (EM (exists e, ~P e)).
- (* there is a non-drinker : it's our witness ! *)
  destruct H as (x,Hx).
  exists x. intros Hx'. destruct Hx. assumption.
- (* everybody drinks : any witness works, in particular e0 *)
  exists e0. intros _. apply notexistsnot. assumption.
Qed.

End Exercise2.

Module Exercise3.

Parameter E:Type.

Definition sets := E -> Prop.

Definition subset (A B : sets) : Prop :=
  forall x:E, A x -> B x.

Lemma subset_refl : forall A, subset A A.
Proof.
unfold subset; intros; assumption.
Qed.

Lemma subset_trans : forall A B C, subset A B -> subset B C -> subset A C.
Proof.
unfold subset; intros.
apply H0; apply H; assumption.
Qed.

Definition eq (A B : sets) : Prop :=
  forall x:E, A x <-> B x.

Lemma eq_refl : forall A, eq A A.
Proof.
 split; intros; assumption.
Qed.

Lemma eq_sym : forall A B, eq A B -> eq B A.
Proof.
unfold eq. intros. split; intros; apply H; assumption.
Qed.

Lemma eq_trans : forall A B C, eq A B -> eq B C -> eq A C.
Proof.
unfold eq. intros. split; intros.
 - apply H0, H; assumption.
 - apply H, H0; assumption.
Qed.

Lemma subset_antisym : forall A B, subset A B /\ subset B A <-> eq A B.
Proof.
 split.
 - intros (H,H'). intros x. split. apply H. apply H'.
 - intros H. split; intro; apply H.
Qed.

Definition union (A B:sets) := fun x => A x \/ B x.
Definition inter (A B:sets) := fun x => A x /\ B x.

Lemma union_com A B : eq (union A B) (union B A).
Proof.
 unfold eq, union.
 firstorder. (* see td1 for a pedestrian proof. *)
Qed.

Lemma inter_com A B : eq (inter A B) (inter B A).
Proof.
 firstorder.
Qed.

Lemma union_idem A : eq (union A A) A.
Proof.
 firstorder.
Qed.

Lemma inter_idem A : eq (inter A A) A.
Proof.
 firstorder.
Qed.

Lemma distr A B C : eq (inter A (union B C)) (union (inter A B) (inter A C)).
Proof.
 firstorder.
Qed.
