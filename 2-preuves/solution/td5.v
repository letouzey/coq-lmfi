Require Import List.
Import ListNotations.

Inductive alpha := M | I | U.

Definition word := list alpha.

Inductive lang : word -> Prop :=
| axiom : lang [M;I]
| rule1 x : lang (x ++ [I]) -> lang (x ++ [I;U])
| rule2 x : lang ([M] ++ x) -> lang ([M] ++ x ++ x)
| rule3 x y : lang (x ++ [I;I;I] ++ y) -> lang (x ++ [U] ++ y)
| rule4 x y : lang (x ++ [U;U] ++ y) -> lang (x ++ y).

Lemma starts_with_M w :
lang w -> match w with
          | letter :: _ => letter = M
          | [] => False
          end.
Proof.
 induction 1. (* the first unnamed hyp, i.e. (lang w) *)
 - reflexivity.
 - destruct x; simpl in *; auto.
 - simpl; reflexivity.
 - destruct x; simpl in *. discriminate.
   auto.
 - destruct x; simpl in *. discriminate.
   auto.
Qed.

(* other possible style for the "match" *)
Lemma starts_with_M_bis w :
lang w -> match w with
          | M :: _ => True
          | _ => False
          end.
Proof.
 induction 1; auto; destruct x; simpl in *; easy.
Qed.

(* Statement via exists. Equivalent with the previous "match" statements,
   but much harder to prove directly (need to exhibit all the witnesses,
   hence all the rest of the words). *)
Lemma starts_with_M_ter w : lang w -> exists v, w = M :: v.
Proof.
 intros H.
 apply starts_with_M in H. destruct w.
 - destruct H.
 - exists w. rewrite H. auto.
Qed.

Inductive Z3 := Z0 | Z1 | Z2.

Notation "0" := Z0.
Notation "1" := Z1.
Notation "2" := Z2.

Definition succ z :=
  match z with
  | 0 => 1
  | 1 => 2
  | 2 => 0
  end.

Definition add z z' :=
 match z with
 | 0 => z'
 | 1 => succ z'
 | 2 => succ (succ z')
 end.

Infix "+" := add.

Compute 2 + 2.

Lemma succ3 : forall x, succ (succ (succ x)) = x.
Proof.
 now destruct x.
Qed.

Lemma add_comm : forall x y, x+y = y+x.
Proof.
 now destruct x, y.
Qed.

Lemma add_assoc : forall x y z, x+(y+z) = (x+y)+z.
Proof.
 now destruct x, y, z.
Qed.

Lemma add_0 x : 0+x = x.
Proof.
 reflexivity.
Qed.

Lemma add_succ x y : succ (x+y) = succ x + y.
Proof.
 now destruct x, y.
Qed.

Lemma twice_nonzero x : x<>0 -> x+x<>0.
Proof.
 now destruct x.
Qed.

Fixpoint occurI3 w : Z3 :=
 match w with
 | [] => 0
 | I::w' => succ (occurI3 w')
 | _::w' => occurI3 w'
 end.

Lemma occurI3_app v w : occurI3 (v ++ w) = occurI3 v + occurI3 w.
Proof.
 induction v; simpl.
 - reflexivity.
 - destruct a; auto.
   rewrite IHv.
   apply add_succ.
Qed.

Lemma lang_occurI3_nonzero w : lang w -> occurI3 w <> 0.
Proof.
 induction 1; simpl in *; rewrite ?occurI3_app in *; simpl in *; try easy.
 - now apply twice_nonzero.
 - now rewrite succ3 in *.
Qed.

Lemma mu_puzzle : ~(lang [M;U]).
Proof.
 intro H.
 apply lang_occurI3_nonzero in H. simpl in H. easy.
Qed.
