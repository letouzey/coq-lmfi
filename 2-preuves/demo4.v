
(** * A few words about the inversion tactics *)

(* Already seen : injection and discriminate :

   injection H  where H : S x = S y gives x = y
   injection H  where H : x::l = x'::l' gives both x=x' and l=l'
   discriminate H where H : O = S x conludes the goal
    (since this case is impossible)
*)

(* The tactic inversion is a generalization of both,
   trying to recover from an inductive predicate
   what situations may have lead to this concrete predicate.
*)

Inductive even : nat -> Prop :=
 | even_O : even O
 | even_SS n : even n -> even (S (S n)).

Lemma even_2 : even 2.
Proof.
 apply even_SS.
 apply even_O.
Qed.

Lemma even_plus4 : forall n, even n -> even (4+n).
Proof.
 intros.
 apply even_SS.
 apply even_SS.
 assumption.
Qed.

(* Up to now, even_2 and even_plus4 are direct proofs,
   no need for inversion *)

Lemma not_even_one : ~even 1.
Proof.
 intro.
 (* Here, destruct H (or induction H) would forget that our number is 1 :-(
    Before destruct H, we need to save all details ourselves
    (for instance via "remember"). *)
 remember 1 as m.
 destruct H.
 - discriminate.
 - discriminate.
Qed.

(* inversion is here nicer than this remember + destruct,
   and way more general *)

Lemma not_even_one_bis : ~even 1.
Proof.
 intro.
 inversion H.
Qed.

Lemma even_plus3 n : even (3+n) -> even (1+n).
Proof.
 intro H.
 inversion H. (* subst. (* if you want to get rid of remaining equations *)*)
 assumption.
Qed.

(* Since equality is also an inductive predicate, inversion also works
   on equality hypothesis (and subsumes injection and discriminate). *)

Lemma test_inj x : S x = 0 -> False.
Proof.
intro H.
inversion H.
Qed.



(** * Impredicative encodings *)

(* Note that we can quantify on all propositions and get a new proposition.
   That's impredicativity. In Coq that's specific to Prop : the Type universe
   is predicative (i.e. not impredicative).
*)

Definition FalseBis : Prop := forall (P:Prop), P.

Lemma False_equiv : False <-> FalseBis.
Proof.
 split.
 - destruct 1.
 - intro H. unfold FalseBis in *.
   apply H.
Qed.


(* Alternative disjunction *)

Definition OrBis (P Q : Prop) : Prop :=
 forall R:Prop, (P -> R) -> (Q -> R) -> R.

Lemma or_equiv P Q : P \/ Q <-> OrBis P Q.
Proof.
 split.
 - destruct 1.
   + unfold OrBis. intros R pr qr. apply pr, H.
   + intros R pr qr. apply qr, H.
 - intro H. unfold OrBis in H. apply H.
   + now left.
   + now right.
Qed.

(* Alternative definition for exists *)

Definition ExistsBis {X}(P:X->Prop)
 := forall Q:Prop, (forall x, P x -> Q) -> Q.

Lemma Exists_equiv {X}(P:X->Prop) :
 (exists x, P x) <-> ExistsBis P.
Proof.
 split.
 - intros (w,H). unfold ExistsBis.
   intros Q H'. apply (H' w), H.
 - intro H. unfold ExistsBis in H. apply H.
   intros w Hw.
   exists w. auto.
Qed.


(* Alternative conjunction *)

Definition AndBis (P Q : Prop) : Prop :=
 forall R:Prop, (P -> Q -> R) -> R.

Lemma and_equiv P Q : P /\ Q <-> AndBis P Q.
Proof.
 split.
 - destruct 1. intros R pqr. apply pqr; auto.
 - intro H. apply H. split; auto.
Qed.
