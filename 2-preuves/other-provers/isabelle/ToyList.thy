theory ToyList
imports Main
begin

(* To avoid conflicts with notations already defined in theory Main *)
no_notation Nil ("[]") and Cons ( infixr "#" 65) and append ( infixr "@" 65)
hide_type list
hide_const rev

datatype 'a list =
  Nil                  ("[]")
| Cons 'a "'a list"    (infixr "#" 65)

primrec app :: "'a list \<Rightarrow> 'a list \<Rightarrow> 'a list" (infixr "@" 65)
where
"[] @ ys       = ys " |
"(x # xs) @ ys = x # (xs @ ys)"

primrec rev :: "'a list \<Rightarrow> 'a list"
where
"rev [] = []" |
"rev (x # xs) = (rev xs) @ (x # [])"

value "rev (True # False # [])"

theorem app_nil_r [simp]: "xs @ [] = xs"
apply (induct_tac xs)
apply (auto)
done

theorem app_ass [simp]: "(xs @ ys) @ zs = xs @ ys @ zs"
apply (induct_tac xs)
apply (auto)
done

theorem rev_app [simp]: "rev(xs @ ys) = rev ys @ rev xs"
apply (induct_tac xs)
apply (auto)
done

theorem rev_rev [simp]: "rev(rev xs) = xs"
apply (induct_tac xs)
apply (auto)
done

(* An experiment with an arithmetical goal (also shows
   how to write hypothesis and conclusion thanks to metalogic) *)

theorem test: "\<lbrakk> ~ m < n; m < n + (1::nat) \<rbrakk> \<Longrightarrow> m = n"
apply (auto)
done

(* NB: "\<lbrakk> .. ; .. \<rbrakk> \<Longrightarrow> .." is a shortcut for ".. \<Longrightarrow> .. \<Longrightarrow> .."
   ASCII notation : "[| .. ; .. |] ==> .."
*)

theorem test': " ~ m < n \<Longrightarrow> m < n + (1::nat) \<Longrightarrow> m = n"
  by auto   (* same as: apply (auto) done *)


(* Optimized definition of rev via an accumulator *)

primrec revapp :: "'a list \<Rightarrow> 'a list \<Rightarrow> 'a list"
where
"revapp [] ys = ys" |
"revapp (x # xs) ys = revapp xs (x # ys)"

definition revopt :: "'a list \<Rightarrow> 'a list"
where
"revopt xs = revapp xs []"

(* Earlier proofs about app and rev were done with "usual" free variables,
   that are fixed through the proof. But lemmas about revapp cannot work
   this way, since the accumulator have to be instantiated to a different
   value during the proof. Solution : write first a generalized version, with
   a \<forall> (or ALL in ASCII). This generalized version can then be used to deduce
   a "usual" version without \<forall>.
*)

theorem revapp_alt_gen [simp]: "\<forall> ys. revapp xs ys = rev xs @ ys"
  apply (induct_tac xs)
  apply (auto)
  done


theorem revopt_rev [simp]: "revopt xs = rev xs"
  apply (simp add:revopt_def)  (* revopt_def is the equation created by "definition rev..." *)
  done

(* Properties about revopt can then be obtained by equivalence with rev *)

theorem revopt_revopt : "revopt (revopt xs) = xs"
  by auto

(* Finally unused lemmas (they can be deduced from above) : *)

theorem revapp_appl: "revapp (xs @ ys) zs = revapp ys (revapp xs zs)"
  by auto

theorem revapp_appr: "revapp xs (ys @ zs) = (revapp xs ys) @ zs"
  by auto

end (* ToyList *)
