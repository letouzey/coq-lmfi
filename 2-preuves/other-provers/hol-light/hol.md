A Brief Description of HOL-Light
================================

HOL : Higher-Order Logic.
HOL-light : one member of a family of provers based on this HOL logic. 

Reference: [HOL Light: an overview. John Harrison](http://www.cl.cam.ac.uk/~jrh13/papers/hollight.pdf)

## The logical framework

All starts with (polymorphic) simply-typed lambda-calculus.

#### The types

 - Polymorphic type variable : `α, β, ...`
 - Two primitive types : `bool` (for boolean, i.e. here all statements) and `ind` (individual, won't use it today).
 - One type constructor : the functional arrow `→`.
 
#### The values

 - Variables `x, y, z, ...`
 - Functions `λx. t`
 - Application `t(u)`
 - Two predefined constants : `(=) : α → α → bool` and `ε : (α → bool) → α` (Hilbert choice operator).

Syntax : `t = u` is just `(((=) t) u)`. 
And when `p` and `q` are of type `bool` (hence propositions), `p = q` is rather written `p <=> q`.

#### The deducibility predicate

Unlike in Coq or Agda, provability is **not** based on a typing relation. The type system is just here to ensure that propositions (and other elements) are well-formed.

So for a proposition `p` (i.e. a well-typed lambda-term of type `bool`) and a context `Γ` (i.e. a set of well-typed lambda-terms of type `bool`), a separate predicate `Γ ⊢ p` will express that `p` is deducible from `Γ`.

This deducibility predicate is obtained through the following ten inference rules


## The logical rules

```

--------- (REFL)
 ⊢ t = t

 Γ ⊢ s = t     ∆ ⊢ t = u
-------------------------- (TRANS)
   Γ ∪ ∆ ⊢ s = u

 Γ ⊢ s = t      ∆ ⊢ u = v
--------------------------- (MK_COMB)
  Γ ∪ ∆ ⊢ s(u) = t(v)

 Γ ⊢ s = t
------------------------- (ABS)
 Γ ⊢ (λx. s) = (λx. t)

----------------- (BETA)
 ⊢ (λx. t)x = t

----------- (ASSUME)
 {p} ⊢ p

 Γ ⊢ p <=> q     ∆ ⊢ p
----------------------- (EQ_MP)
 Γ ∪ ∆ ⊢ q

 Γ ⊢ p           ∆ ⊢ q
--------------------------------- (DEDUCT_ANTISYM_RULE)
 (Γ − {q}) ∪ (∆ − {p}) ⊢ p <=> q

Γ[x1,...,xn] ⊢ p[x1,...,xn]
---------------------------- (INST)
Γ[t1,...,tn] ⊢ p[t1,...,tn]

Γ[α1,...,αn] ⊢ p[α1,...,αn]
---------------------------- (INST_TYPE)
Γ[γ1,...,γn] ⊢ p[γ1,...,γn]

```

Actually, three extra axioms :

- Extensionality via `ETA_AX` : `⊢ (λx.(t x)) = t`.
- `SELECT_AX` stating that Hilbert operator `ε` is a choice operator : `⊢ P x ==> P(ε(P))`. See below for the definition of implication `==>`. This axiom is the only one that implies that HOL-Light is a classical logic.
- `INFINITY_AX` stating that the `ind` type of individuals is infinite.

## The other logical connectives

On top of predefined equality `=` (and hence equivalence `<=>` on propositions), the other logical connectives are "syntactic sugar" :

```
T       := (λp. p) = (λp. p)
p /\ q  := (λf. f p q) = (λf. f T T)
p ==> q := (p /\ q) <=> p
∀P      := P = (λx. T)
∃P      := ∀q. (∀x. P(x) ==> q) ==> q
p ‌\/ q  := ∀r. (p ==> r) ==> (q ==> r) ==> r
⊥       := ∀p. p
¬p      := p ==> ⊥
```
Note : the encodings for exist and disjunction and false are called impredicative encodings.

Note that `∀x.t` (actually written `!x.t` in HOL-Light syntax) is a notation for `∀(λx.t)`. Here, `t` can be any term of type `bool`, with occurrences of `x` in it or not. Similarly, `∃x.t` (written `?x.t` in HOL-Light) is `∃(λx.t)`.

The HOL-Light syntax for `⊥` is `F` and `¬` is `~`.

## Some elementary proofs

#### Symmetry of equality

The symmetry rule (`Γ ⊢ s = t` implies `Γ ⊢ t = s`) is admissible (i.e. can be "emulated" via the other rules).

```
------------ REFL
⊢ (=) = (=)          Γ ⊢ s = t
------------------------------ MK_COMB   --------- REFL
Γ ⊢ ((=) s) = ((=) t)                     ⊢ s = s
-------------------------------------------------- MK_COMB   --------- REFL
Γ ⊢ (s = s) <=> (t = s)                                       ⊢ s = s
---------------------------------------------------------------------- EQ_MP
Γ ⊢ t = s

```

#### Transitivity

Transitivity is one of the basic rules (for efficiency reasons). But it can actually be derived from the other rules. 

```
---------------------- REFL
 ⊢ ((=) s) = ((=) s)          Δ ⊢ t = u
---------------------------------------- MK_COMB
Δ ⊢ (s = t) <=> (s = u)                           Γ ⊢ s = t
------------------------------------------------------------ EQ_MP
Γ ∪ ∆ ⊢ s = u

```

#### Natural deduction

Exercise : retreive the usual introduction and elimination rules for `T`, `/\`, `==>`, `∀`, `∃`, `\/`, `⊥`, `¬`.


Truth introduction :

```
--------------------REFL
⊢ (λp. p) = (λp. p)
=
⊢ T
```

Side note : `|- p ` and `|- p = T` are equivalent :

```
|- p            |- T
-----------------------DEDUCT_ANTISYM_RULE
|- p=T


|- p=T
--------SYM
|- T=p            |- T
------------------------EQ_MP
|- p

```

Conjunction introduction :

```
          |-p
-------R  ------(See above)
|- f=f    |-p=T               |- q
-----------------MK_COMB     ----------(See above)
|- f p = f T                  |- q = T
---------------------------------------MK_COMB
|- f p q = f T T
---------------------------------ABS
|- (λf. f p q) = (λf. f T T)
=
|- p /\ q

```

Conjunction left elimination :

```
|- p/\q
=                                   ----------REFL
|- (λf. f p q) = (λf. f T T)         |- f = f
----------------------------------------------- MK_COMB
|- (λf. f p q) f = (λf. f T T ) f
(... TRANS + BETA ...)
    ||                  ||
|-  f p q        =  f T T
------------------------------------ INST
|- (λxy.x) p q   = (λxy.x) T T
(...beta conversion, see below...)
|- p = T
---------(see before)
|-p
```

NB : we indeed have a more general beta conversion, via `BETA+INST` :
```
------------(BETA)
⊢ (λx.t)x = t
---------------------------(INST)
⊢ ((λx.t)x)[x/u] = t[x/u]
=
⊢ (λx.t)u = t[x/u]
```


Exercise(++) : retreive at least one classical law from `SELECT_AX`.

Note: weakening can be done via an intermediate implication:

```
            -----ASSUME
  |- B      A|-A
------------------- (/\-i)    --------- (/\-e2)
A |- A/\B                     A/\B |- A
----------------------------------------
  |- A/\B<=>A    -----ASSUME
  |- A==>B       A|-A
------------------------(==>-e)
A |- B
```
