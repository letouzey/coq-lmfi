TD1 : Premieres preuves Coq
===========================

M2 LMFI

Pierre Letouzey (d'après A. Miquel)

> La documentation du système Coq est consultable en ligne: http://coq.inria.fr/doc/

### Révision : Démarrage du système ###

Il existe actuellement plusieurs manières principales d'utiliser Coq:

 * via `coqide`, une interface graphique basée sur `gtk`
 * via `proofgeneral`,  qui est un mode pour `emacs`
 * directement dans son navigateur en utilisant `jsCoq`, p.ex. https://jscoq.github.io/node_modules/jscoq/examples/scratchpad.html
   (voir https://github.com/ejgallego/jscoq pour le mode d'emploi)
 * ou éventuellement en lançant `coqtop`, une boucle d'interaction textuelle à la `ocaml`

Chaque méthode a ses aficionados (même la dernière).

Par convention, les fichiers Coq utilise l'extension `.v` (pour "vernaculaire"...).

Une fois lancées, les interfaces `coqide`, `proofgeneral` et `jsCoq` proposent une disposition assez similaire : le fichier en cours d'édition est à gauche, tandis que les preuves en cours seront affichées en haut à droite, et les messages du système en bas à droite (réponses de Coq ou messages d'erreurs). La portion du fichier déjà soumise à Coq est indiquée par des couleurs, et ces interfaces permettent de faire descendre cette zone, en envoyant une ou plusieurs phrases à Coq, ou au contraire de remonter (retour à un ancien état de Coq).

### Révision : Commandes Coq ###

En Coq, une commande est formée d'un nom de commande (commençant par une majuscule), éventuellement suivie d'un ou plusieurs arguments, et terminée par un point.  Exemples:
```
  Check 0.
  Check S.
  Check nat.
  Print nat.
  Search nat.
  Check 2 + 2 = 5.
  Check forall x, exists y, x = 2 * y \/ x = 2 * y + 1.
  Definition id := fun (A : Set) (x : A) => x.
  Check id.
  Check id nat 7.
```

Après avoir tapé quelques commandes, soumettez-les à Coq, en utilisant l'un des moyens de navigation (icônes, menus ou raccourcis clavier). Observez le déplacement de la zone colorée marquant la partie du fichier déjà exécutée.

### Passage au mode preuve ###

L'utilisateur déclare son intention d'effectuer une preuve avec une commande de la forme :
```
Lemma and_commut :
 forall A B : Prop, A /\ B <-> B /\ A.
```
On notera que cette commande donne un nom (ici: `and_commut`) au lemme, ce qui permettra de le référencer par la suite. On peut éventuellement remplacer `Lemma` par `Theorem` ou quelques autres mots-clés équivalents comme `Proposition` ou `Fact`. On marquera le début de la preuve par la commmande `Proof` (très fortement recommandée même si facultative).

### Sous-buts et tactiques ###

Une fois le mode preuve lancé, la zone supérieure droite affiche en permanence un ou plusieurs sous-buts (**subgoals**) qu'il s'agit de démontrer. Ces sous-buts sont essentiellement des séquents de la déduction naturelle écrits verticalement: les hypothèses (nommées) sont en haut, et la conclusion figure en bas sous un trait. Dans la partie haute figurent également des déclarations de variables.

La preuve se fait à l'aide de **tactiques** (distinguées des commandes par une minuscule initiale), qui effectuent des transformations plus ou moins complexes sur le but courant.
Par exemple, la tactique `intro` effectuée sur un but de la forme `A -> B` introduit une hypothèse `H : A` dans le contexte, et remplace la conclusion par `B`. À chaque règle d'inférence de la déduction naturelle correspond une ou plusieurs tactiques, mais certaines tactiques permettent également d'effectuer des morceaux de preuve plus complexes, comme par exemple la résolution de contraintes linéaires en arithmétique de Presburger (tactique `omega`).

Les tactiques sont susceptibles d'engendrer de nouveaux sous-buts (correspondant aux prémisses), ou au contraire de faire disparaître le but courant (lorsque celui-ci est résolu).
La preuve est terminée lorsqu'il n'y a plus de sous-but à démontrer. On doit alors utiliser la commande `Qed` (d'après *Quod erat demonstrandum*, le CQFD latin) pour conclure la preuve et repasser au mode *commande*. Voici par exemple une preuve complète pour l'énoncé précédent :

```
Lemma and_commut :
  forall A B : Prop, A /\ B <-> B /\ A.
Proof.
 intros. split.
 - intros. destruct H. split. assumption. assumption.
 - intros. destruct H. split; assumption.
Qed.
```

Lorsque les tactiques sont séparées par des points, Coq va alors les exécuter pas-à-pas. On peut aussi utiliser le `;` pour **chaîner** des tactiques. Ainsi `split;assumption`
fait agir `assumption` sur les deux sous-buts créés par `split`.

Devant des tactiques, on peut éventuellement placer une **puce**, c'est-à-dire un des marqueurs `-` ou `+` ou `*`. Ces puces sont optionnelles mais aident grandement à hiérarchiser la preuve en cours en délimitant chaque sous-partie.

Un tel *script de preuve*, une fois sauvé dans un fichier tel que `mes_preuves.v`, peut 
ensuite être *compilé* via la commande unix `coqc mes_preuves.v`.
Si les preuves que contient ce fichier sont correctes, un fichier compilé  `mes_preuves.vo` est alors produit, qui permettra de recharger ces preuves rapidement par la suite (cf. la commande `Require Import`).

### Premières tactiques élémentaires ###

- `assumption` si le but à prouver est déjà en hypothèse (cf. la *règle axiome* en logique).
- Pour les opérateurs primitifs (quantification `∀`, implication `->`):
   * introduction via `intro` (ou ses variantes `intros`, `intro x`, `intros x y ...`)
   * élimination via `apply H` (si `H` est le nom de l'hypothèse à éliminer)
- Pour les autres opérateurs (qui sont en fait des définitions inductives), en théorie on introduit avec `constructor` et on élimine avec `destruct H`.
  Mais l'introduction nécessite souvent des tactiques dédiées:
   * `split` pour une conjonction `/\`
   * `left` et `right` pour une disjonction `\/`
   * `exists ...` pour une quantification `∃` (et les `...` doivent donner le témoin)
   * Aucune tactique d'introduction pour `False` !
   * Pour `True` (rarement utilisé en Coq), l'introduction peut se faire via `constructor`, par contre il n'y a rien à éliminer.
- Abbréviations:
  * Une négation `~A` n'est qu'une abbréviation pour `A->False`. Donc introduction via `intro` (ou `intros a`, mettre un nom pour forcer l'introduction), et élimination via `apply H` ou `destruct H` (selon qu'on s'intéresse au `->` ou au `False` sous-jacent).
  * Une équivalence `A<->B` n'est qu'une abbréviation pour `(A->B)/\(B->A)`, donc se manipule comme une conjonction.
- Quelques premières tactiques automatiques : `trivial`, `easy`, `auto`, `eauto`, `intuition`, `firstorder`. Voir la documentation de Coq pour plus d'information. S'entraîner tout d'abord à savoir faire sans ces tactiques. En effet, si les dernières tactiques de la liste peuvent résoudre les exercices ci-dessous, elles n'aident pas forcément tant que cela sur des preuves plus délicates, et il pourra être utile alors de savoir procéder par petits pas.

### Exercice 1 : Calcul propositionnel ###

Poser l'existence de variables propositionnelles `A`, `B` et `C` via la commande:
```
Parameters A B C : Prop.
```
Puis prouver en Coq les formules suivantes :
```
Lemma E1F1 : A -> A.
Lemma E1F2 : (A -> B) -> (B -> C) -> A -> C.
Lemma E1F3 : A /\ B <-> B /\ A.
Lemma E1F4 : A \/ B <-> B \/ A.
Lemma E1F5 : (A /\ B) /\ C <-> A /\ (B /\ C).
Lemma E1F6 : (A \/ B) \/ C <-> A \/ (B \/ C).
Lemma E1F7 : A -> ~~A.
Lemma E1F8 : (A -> B) -> ~B -> ~A.
Lemma E1F9 : ~~(A \/ ~A).
```

### Exercice 2 : Calcul des prédicats ###

Après avoir effectué les déclarations suivantes:
```
Parameter X Y : Set.
Parameter P Q : X -> Prop.
Parameter R : X -> Y -> Prop.
```
Prouver en Coq les formules suivantes:
```
Lemma E2F1 : (forall x, P x /\ Q x) <-> (forall x, P x) /\ (forall x, Q x).
Lemma E2F2 : (exists x, P x \/ Q x) <-> (exists x, P x) \/ (exists x, Q x).
Lemma E2F3 : (exists y, forall x, R x y) -> forall x, exists y, R x y.
```

### Exercice 3 : Formalisation ###

Enoncer en Coq et prouver quelques questions des exercices 1 et 4 de ce [TD de logique](https://gitlab.math.univ-paris-diderot.fr/letouzey/cours-preuves/blob/master/td1.pdf). Pour simuler la règle de raisonnement par l'absurde, on pourra déclarer l'axiome suivant:
```
Axiom not_not_elim : forall A : Prop, ~~A -> A.
```
