TD1bis : Elementary mathematics in Coq
======================================

M2 LMFI

Pierre Letouzey (from A. Miquel)

### Some more tactics ###

In Coq, a definition `d` could be replaced by its body thanks to the tactic `unfold d`.

The equality is handled by tactics `reflexivity` and `symmetry` and `transitivity ...` (give the intermediate term) and `rewrite ...` (give the equality name or lemma to use as a rewrite rule) or `rewrite <- ...` (right-to-left rewriting).

### Exercise 1 : Order relations ###

Let's consider a type `E:Type` equipped by a binary relation `R` supposed to satisfy the axioms of order relations:

```coq
Parameter E : Type.
Parameter R : E -> E -> Prop.
Axiom refl : forall x : E, R x x.
Axiom trans : forall x y z : E, R x y -> R y z -> R x z.
Axiom antisym : forall x y : E, R x y -> R y x -> x = y.
```

We define the notion of smallest and minimal elements this way:

```coq
Definition smallest (x0 : E) := forall x : E, R x0 x.
Definition minimal (x0 : E) := forall x : E, R x x0 -> x = x0.
```

What are the types of `smallest` and `minimal` ?

State in Coq and prove the following lemmas:

  1. If `R` admits a smallest element, this one is unique.
  2. The smallest element, if it exists, is a minimal element.
  3. If `R` admits a smallest element, then there is no other minimal element that this one.

### Exercice 2 : Classical logic ###

In this exercise, we assume the reasoning rule "by absurdum", that we can declare in Coq via:

```coq
Axiom not_not_elim : forall A : Prop, ~~A -> A.
```

  1. Show in Coq that this axiom implies the excluded-middle : `forall A : Prop, A \/ ~ A`.

We now aim at proving the drinker's paradox (due to Smullyan):

> In an non-empty room we can find someone with the following property:
> if this person drinks, then everybody in the room drink.

  2. Declare in Coq the needed elements to formalize the problem (cf. the previous exercise).
  
  3. State and prove this paradox (thanks to excluded-middle).

### Exercice 3 : Subsets ###

Given a type `E:Type` in Coq, we consider subsets of `E`, represented here as unary predicates on `E`, i.e. objects of type `E->Prop`.
  
  1. Define in Coq the binary predicate `subset : (E->Prop)->(E->Prop)->Prop` expressing the inclusion of two subsets. Show that this relation is reflexive and transitive. Is is antisymmetric ?

  2. Define in Coq a binary predicate `eq : (E->Prop)->(E->Prop)->Prop` expressing the extensional equality of two subsets. Show that it is indeed a equivalence relation. Show that `subset` is antisymmetric with respect of `eq`.

  3. Define in Coq the union and intersection operators on subsets of `E`. Show that theses operations are associative, commutative, idempotent and distributive (one on the other).
