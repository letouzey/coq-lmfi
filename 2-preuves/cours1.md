Cours de Preuves Assistées par Ordinateurs - Seance 1
=====================================================

## Quelques problèmes mathématiques célèbres

#### Théorème des 4 couleurs

<https://en.wikipedia.org/wiki/Four_color_theorem>

 - Une carte plane et pas trop irrégulière (pas d'enclaves, de frontières fractales...) peut se colorier avec au plus 4 couleurs sans que deux pays avec une frontière commune aient la même couleur.

 - Enoncé : 1852
 
 - Pleins de preuves fausses ensuite...
 
 - Preuve complète : 1976 par K. Appel et W. Haken
 
 - Premier théorème important utilisant l'ordinateur, ~2000 sous-cas finis traités en ~100h de calculs à l'époque
 
 - Pour beaucoup de mathématiciens, ceci n'était pas une preuve (les calculs sont-ils fiables ?)
 
 - Preuve unifiée en Coq : 2005 par Georges Gonthier (et B. Werner). Raisonnement infini + calcul de 600 cas finis.
   50k lignes de code Coq : <https://github.com/math-comp/fourcolor>

#### Théorème de Feit-Thompson

<https://en.wikipedia.org/wiki/Feit%E2%80%93Thompson_theorem>

Tout groupe fini d'ordre impair est résoluble.

  - Conjecture : Burnside 1911
  
  - Démonstration : Feit et Thompson 1963 (250 pages)
  
  - Etape essentielle pour la classification des groupes finis simples.
    Fait intervenir beaucoup d'algèbre moderne.

  - Preuve Coq : 2006-2012 G. Gonthier et son équipe (40k lignes)
    <https://github.com/math-comp/odd-order>


#### Conjecture de Kepler

<https://en.wikipedia.org/wiki/Kepler_conjecture>

Maintenant Théorème de Kepler...

La manière la plus compacte de disposer des sphères dans l'espace est
CFC (Cubique Face Centrée) ou ses variantes (p.ex. HC hexagonal compact).
Densité obtenue : `π/√18` (environ 74%)

Enoncé : 1611

Preuve : Thomas Hales 1998 (+100 pages, 3Go de programmes et données)

Publication : seulement 2006, avec avertissement des relecteurs (99% confiants, mais pas sur les calculs informatiques)

Preuve formelle : projet flyspeck 2003-2014.
HOL Light (avec des bouts intermediaires en Isabelle, Coq utilisé aussi à un moment).
Un morceau demandant ~5000h CPU à l'époque ! Le reste de l'ordre de l'heure CPU.

<https://github.com/flyspeck/flyspeck>
<https://arxiv.org/abs/1501.02155>


#### Théorème de Fermat

<https://en.wikipedia.org/wiki/Fermat%27s_Last_Theorem>

Enoncé : Fermat, vers 1670

Preuve : Andrew Wiles en 1994.

Utilise directement ou indirectement une large partie des mathématiques du 20e siècle (courbes elliptiques, ...).

Preuve formelle : non


## Au fait, il reste encore des conjectures à prouver !

#### Conjecture de Goldbach

<https://fr.wikipedia.org/wiki/Conjecture_de_Goldbach>

Tout nombre entier pair > 3 peut s’écrire comme la somme de deux nombres premiers.

Enoncé : C. Goldbach (1742)

#### Conjecture de Collatz (ou de Syracuse)

<https://en.wikipedia.org/wiki/Collatz_conjecture>

En partant d'un entier > 0, et en répétant le processus suivant, on atteint toujours 1 un jour : 
division par deux des nombres pairs et passage à `3x+1` pour les autres nombres `x`.

Enoncé : Lothar Collatz (1937)

#### Hypothèse de Riemann

<https://en.wikipedia.org/wiki/Riemann_hypothesis>

## Du côté Informatique

## Complexité de gros projets informatiques modernes

 - Noyau linux : 20 Mloc
 - GCC : 7 Mloc
 
En comparaison, Coq c'est 270kloc de sources OCaml et 230 kloc de bibliothèques de preuves.
Et son noyau sur lequel repose la validation des preuves c'est environ 30 kloc.

#### Projet Compcert

 Compilateur certifié de C vers différents assembleurs (ppc, arm, x86, x86_64, RISC-V).
 
 Quelques détails de C manquent (mais traite 99% de la norme).
 
 Efficacité proche de `gcc -O1`, nettement meilleur que `gcc -O0`.

 Auteur : Xavier Leroy et son équipe

 Première version : 2008
 
 Taille : environ 180k lignes de Coq

<https://en.wikipedia.org/wiki/CompCert>

## Des situations plus quotidiennes

#### split

Au détour d'un autre cours, une preuve d'équivalence de deux fonctions OCaml:

```ocaml
let rec split1 = function
 | [] -> ([],[])
 | a::l -> let (l1,l2) = split1 l in (a::l2,l1)

let rec split2 = function
 | [] -> ([],[])
 | [a] -> ([a],[])
 | a::b::l -> let (l1,l2) = split2 l in (a::l1,b::l2)
```

#### Une fonction d'Hofstadter

<https://www.irif.fr/~letouzey/hofstadter_g/>

<https://hal.inria.fr/hal-01195587/document>

Au départ, une "simple" étude de la fonction récursive `g(n) = n - g(g(n-1))` (avec valeur initiale `g 0 = 0`).
Mais on tombe vite sur des choses délicates (multiples cas, plusieurs pages de preuves).
Et au moins un article publié sur le sujet (en 1986) a son résultat principal totalement faux!
Donc ici Coq vient en renfort pour s'assurer que les preuves de cette étude sont justes

#### Sudoku

Un bon exemple pour montrer que même sur un domaine fini, tout traduire en formules booléennes et lancer un sat-solver peut donner des résultats rapides, mais qui peuvent être difficiles à relire et à croire par un humain (formules à milliers de variables, programmes boites-noires ne détaillant pas pourquoi une formule est "unsat").
