Cours de Coq (M2 LMFI, Université de Paris)
===========================================

- [Partie 1 : Programmation fonctionnelle en Coq](1-prog)
- [Partie 2 : Preuves formelles en Coq](2-preuves)
